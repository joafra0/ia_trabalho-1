﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

public class Jogador : MonoBehaviour
{
	NavMeshAgent	_navMesh;

	static Jogador	_instancia;

	void Awake()
    {
		_instancia = this;
	}
	
	void Start ()
    {
		_navMesh = GetComponent<NavMeshAgent>();
	}
	
	void Update ()
    {
		if (Input.GetMouseButtonDown(0))
        {
			Ray raio = Camera.main.ScreenPointToRay(Input.mousePosition);

			RaycastHit golpe;
			if (Physics.Raycast(raio, out golpe))
            {
				_navMesh.SetDestination(golpe.point);
			}
		}
	}

	public static Jogador instancia
    {
		get { return _instancia; }
	}
}
