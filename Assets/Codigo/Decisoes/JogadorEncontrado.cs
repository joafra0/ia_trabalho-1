﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JogadorEncontrado : Decision
{
	Personagem	_personagem;

	public JogadorEncontrado(IDecisionTreeNode trueNode, IDecisionTreeNode falseNode, Personagem personagem)
		: base(trueNode, falseNode)
    {
			_personagem = personagem;
	}

	protected override bool Test()
    {
		return _personagem.jogadorEncontrado != null;
	}
}
