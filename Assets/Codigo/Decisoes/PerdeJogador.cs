﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerdeJogador : Decision
{

    Jogador _jogEncontrado;


	public PerdeJogador(IDecisionTreeNode trueNode, IDecisionTreeNode falseNode, Jogador jogEncontrado)
		: base(trueNode, falseNode)
    {
        _jogEncontrado = jogEncontrado;
	}

	protected override bool Test()
    {
        return !_jogEncontrado;
    }
}
