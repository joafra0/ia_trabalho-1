﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parede : MonoBehaviour
{
    [SerializeField]
    Transform   _proxDestino;

    public Transform proxDestino
    {
        get { return _proxDestino; }
    }
}
