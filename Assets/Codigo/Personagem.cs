﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

public enum ActionType { PATRULHAR, PERSEGUIR }

class Acao : IDecisionTreeNode
{
	public ActionType acao; 

	public Acao(ActionType acao)
    {
		this.acao = acao;
	}

	public IDecisionTreeNode MakeDecision()
    {
		return this;
	}
}

public class Personagem : MonoBehaviour
{
    const float _TEMPO_PERSEGUIR = 3;

    [SerializeField]
    Transform   _destinoAtual;

	NavMeshAgent	_navMesh;

	Decision	_decTopo;

	public Jogador  jogadorEncontrado;

    float _tempoPerseguir;

    ActionType  _acaoAnterior;
	
	void Start()
    {
		_navMesh = GetComponent<NavMeshAgent>();

        _acaoAnterior = ActionType.PERSEGUIR; // Começa nesta

		Acao patrulhar = new Acao(ActionType.PATRULHAR);
		Acao perseguir = new Acao(ActionType.PERSEGUIR);

        _decTopo = new JogadorEncontrado(perseguir, patrulhar, this);
	}

	public void ExecutarAcao(ActionType acao)
    {
        Jogador j;
		switch(acao)
        {
			case ActionType.PATRULHAR:
                if (_acaoAnterior != acao) _navMesh.SetDestination(_destinoAtual.position);
                Parede p = DetetarObstaculos<Parede>(3);
                if (p) {
                    _destinoAtual = p.proxDestino;
                    _navMesh.SetDestination(_destinoAtual.position);
                }
                else
                {
                    j = DetetarObstaculos<Jogador>(7);
                    if (j) jogadorEncontrado = j;
                }
                break;
            case ActionType.PERSEGUIR:
                if (_acaoAnterior != acao) _tempoPerseguir = 0;
                _navMesh.SetDestination(jogadorEncontrado.transform.position);
                j = DetetarObstaculos<Jogador>(8);
                if (j && jogadorEncontrado) {
                    _tempoPerseguir += Time.deltaTime;
                    if (_tempoPerseguir >= _TEMPO_PERSEGUIR) jogadorEncontrado = null;
                }
                else {
                }
                break;
        }
        _acaoAnterior = acao;
	}

    public T DetetarObstaculos<T>(float distancia)
    {
        RaycastHit raio;

        //Debug.DrawRay(transform.position, transform.forward);

        dist = distancia;
        if (Physics.SphereCast(transform.position, distancia, transform.forward, out raio, Mathf.Infinity))
        {
            T objeto = raio.collider.GetComponent<T>();
            return objeto;
        }
        return default(T);
    }

    float dist;

    void OnDrawGizmos() {
        Gizmos.DrawWireSphere(transform.position, dist);
    }
	
	void Update()
    {
        Acao a = _decTopo.MakeDecision() as Acao;
		ExecutarAcao(a.acao);
        //print(_tempoPerseguir);
	}
}
